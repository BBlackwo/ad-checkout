import { companies } from './../companies';
import { Component, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'app-total',
  templateUrl: './total.component.html',
  styleUrls: ['./total.component.css']
})
export class TotalComponent {

  totalPrice = 0;

  @Input() classicQt: any = 0;
  @Input() standoutQt: any = 0;
  @Input() premiumQt: any = 0;

  @Input() selectedCompanyId: any = 0;

  getTotalPrice() {
    const company = companies.filter((c) => c.id === this.selectedCompanyId)[0];
    return company && company.calculatePrice(this.classicQt, this.standoutQt, this.premiumQt);
  }

}
