import { CompanyTypes } from './../companies';
import { Component, Output } from '@angular/core';
import { companies } from '../companies';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.css']
})
export class CompanyComponent {
  selectedCompanyId = CompanyTypes.None;
  companies = companies;

  @Output() companySelected: BehaviorSubject<number> = new BehaviorSubject(this.selectedCompanyId);

  onCompanySelected() {
    this.companySelected.next(this.selectedCompanyId);
  }
}
