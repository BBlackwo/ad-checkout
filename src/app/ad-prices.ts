export enum AdTypes {
  Classic,
  Standout,
  Premiun,
}

export const prices = {
  classic: {
    id: AdTypes.Classic,
    name: 'Classic Ad',
    price: 269.99
  },
  standout: {
    id: AdTypes.Standout,
    name: 'Standout Ad',
    price: 322.99
  },
  premium: {
    id: AdTypes.Premiun,
    name: 'Premium Ad',
    price: 394.99
  }
};

export const specialPrices = {
  Unilever: {
    numPriceOf2: 3
  },
  Apple: {
    standoutPrice: 299.99
  },
  Nike: {
    premiumOver4Price: 379.99
  },
  Ford: {

  },
};
