import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  classicQt: number;
  standoutQt: number;
  premiumQt: number;

  selectedCompanyId: number;

  onCompanySelected(selectedCompanyId) {
    this.selectedCompanyId = selectedCompanyId;
  }
}
