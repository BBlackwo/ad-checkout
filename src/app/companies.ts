import { prices, specialPrices } from './ad-prices';

export enum CompanyTypes {
  None,
  Unilever,
  Apple,
  Nike,
  Ford
}

export const companies = [
  {
    name: 'None',
    id: CompanyTypes.None,
    calculatePrice: (classicQt = 0, standoutQt = 0, premiumQt = 0) => {
      return classicQt * prices.classic.price +
        standoutQt * prices.standout.price +
        premiumQt * prices.premium.price;
    }
  },
  {
    name: 'Unilever',
    id: CompanyTypes.Unilever,
    // - Gets a for **3 for 2 deal on Classic Ads**
    calculatePrice: (classicQt = 0, standoutQt = 0, premiumQt = 0) => {
      const discountCost = Math.floor(classicQt / specialPrices.Unilever.numPriceOf2) * (prices.classic.price * 2);
      const remainderCost = (classicQt % specialPrices.Unilever.numPriceOf2) * prices.classic.price;
      const classicPrice = discountCost + remainderCost;

      const standoutPrice = standoutQt * prices.standout.price;
      const premiumPrice = premiumQt * prices.premium.price;

      return classicPrice + standoutPrice + premiumPrice;
    }
  },
  {
    name: 'Apple',
    id: CompanyTypes.Apple,
    // - Gets a discount on **Standout Ads where the price drops to $299.99 per ad**
    calculatePrice: (classicQt = 0, standoutQt = 0, premiumQt = 0) => {
      return classicQt * prices.classic.price +
        standoutQt * specialPrices.Apple.standoutPrice +
        premiumQt * prices.premium.price;
    },
  },
  {
    name: 'Nike',
    id: CompanyTypes.Nike,
    // - Gets a discount on **Premium Ads when 4 or more** are purchased. The price drops to **$379.99 per ad**
    calculatePrice: (classicQt = 0, standoutQt = 0, premiumQt = 0) => {
      let premiumAdCost = prices.premium.price;

      if (premiumQt >= 4) {
        premiumAdCost = specialPrices.Nike.premiumOver4Price;
      }

      return classicQt * prices.classic.price +
        standoutQt * prices.standout.price +
        premiumQt * premiumAdCost;
    },
  },
  {
    name: 'Ford',
    id: CompanyTypes.Ford,
    // - Gets a **5 for 4 deal on Classic Ads**
    // - Gets a discount on **Standout Ads where the price drops to $309.99 per ad**
    // - Gets a discount on **Premium Ads when 3 or more** are purchased. The price drops to **$389.99
    calculatePrice: (classicQt = 0, standoutQt = 0, premiumQt = 0) => {
      return 0; // didn't bother to implement as it's the same as others
    },
  }
];
